<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Contracts\Validation\Factory as Validator;

class WebShopServiceProvider extends ServiceProvider
{
    /**
     * Validator instance.
     *
     * @var \Illuminate\Contracts\Validation\Factory
     */
    protected $validator;

    /**
     * Bootstrap the application services.
     *
     * @param Validator $validator
     * @return void
     */
    public function boot(Validator $validator)
    {
        $this->validator = $validator;

        $this->registerMorphMaps();

        $this->loadCustomValidators();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Custom model relationship morph maps.
     *
     * @return void
     */
    protected function registerMorphMaps()
    {
        Relation::morphMap([
            'User'    => User::class,
            'Order'   => Order::class,
            'Product' => Product::class,
        ]);
    }

    /**
     * Load the custom validator methods.
     *
     * @return void
     */
    protected function loadCustomValidators()
    {
        $customValidatorClass = 'App\Recruitment\Validators\CustomValidators';

        $this->extendValidator('greater_than', $customValidatorClass);
        $this->extendValidator('mobile_number', $customValidatorClass);
        $this->extendValidator('text_without', $customValidatorClass);
        $this->extendValidator('dimension_option', $customValidatorClass);
        $this->extendValidator('image_dimension', $customValidatorClass);
        $this->extendValidator('numeric_max', $customValidatorClass);
        $this->extendValidator('numeric_min', $customValidatorClass);
        $this->extendValidator('otp', $customValidatorClass);
        $this->extendValidator('uuid', $customValidatorClass);
        $this->extendValidator('decimal', $customValidatorClass);
        $this->extendValidator('duration', $customValidatorClass);
        $this->extendValidator('custom_file', $customValidatorClass);
        $this->extendValidator('sort_by', $customValidatorClass);
        $this->extendValidator('boolean_query', $customValidatorClass);
    }

    /**
     * Extend the validator with custom methods.
     *
     * @param string $name
     * @param string $class
     * @return void
     */
    protected function extendValidator($name, $class)
    {
        $method = 'validate' . studly_case($name);

        $this->validator->extend($name, "{$class}@{$method}");
    }
}
