<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\PayOrderRequest;
use App\Transformers\OrderTransformer;
use App\Http\Requests\ProductOrderRequest;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends ApiController
{
    /**
     * Create an Order.
     *
     * @param OrderRequest $request
     * @return JsonResponse
     */
    public function store(OrderRequest $request)
    {
        $data = [
            'customer_id' => $request->input('customer_id'),
            'payed'       => $request->input('payed', 0),
        ];

        $order = Order::create($data);

        return $this->respondCreated($this->transform($order));
    }

    /**
     * Get Order Details
     *
     * @param Order $order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Order $order, Request $request)
    {
        $data = $this->transform($order);

        return $this->respondSuccess($data);
    }

    /**
     * List all orders.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit', 15);

        $orders = Order::orderBy('id')
            ->limit($limit);

        $data = filter($orders, new OrderTransformer())
            ->paginate();

        return $this->respondOk($data, "Ok");
    }


    /**
     * Update an order.
     *
     * @param OrderRequest $request
     * @param Order $order
     * @return JsonResponse
     */
    public function update(OrderRequest $request, Order $order)
    {
        $customerId = $request->input('customer_id');
        $payed = $request->input('payed', 0);

        if ($customerId) {
            $order->update([
                'customer_id' => $customerId,
                'payed'       => $payed,
            ]);
        }

        return $this->respondSuccess($this->transform($order));
    }

    /**
     * Delete an order.
     *
     * @param Order $order
     * @return JsonResponse
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return $this->respondNoContent();
    }

    /**
     * Attach Product to Order.
     *
     * @param Order $order
     * @param Request $request
     * @return JsonResponse
     */
    public function addProduct(ProductOrderRequest $request, $orderId)
    {

       $order = Order::find($orderId);

       if (empty($order)) {
           return $this->respondError("Order not Found", Response::HTTP_BAD_REQUEST);
       }

        if ($order->payed) {
            return $this->respondOk("Already paid");
        }

        DB::table('product_orders')->insert([
            'order_id'   => $order->id,
            'product_id' => $request->input('product_id'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        return $this->respondSuccess("Product attached to order successfully");

    }

    /**
     * Pay for the order.
     *
     * @param PayOrderRequest $request
     * @return JsonResponse
     */
    public function payOrder(PayOrderRequest $request, $orderId)
    {
        $order = Order::find($orderId);

        if (empty($order)) {
            return $this->respondError("Order not Found", Response::HTTP_BAD_REQUEST);
        }

        $data = [
            'order_id'       => $orderId,
            'customer_email' => $request->input('email'),
            'value'          => $request->input('price'),
        ];

        $config = config('webshop');
        $url = data_get($config, 'pay_url');

        $response = http_client()
            ->setJsonResponseType()
            ->setHeaders(['Content-Type' => 'text/plain'])
            ->post($url, $data);

        $code = data_get($response, 'code');
        $message = data_get($response, 'message');

        if ($code == 200 && $message == 'Payment Successful') {

            $order->update(['payed' => 1]);

            $this->respondSuccess($message);
        }

        return $this->respondError("Insufficient Funds", Response::HTTP_BAD_REQUEST);

    }

    /**
     * Transform the Order.
     *
     * @param Order $order
     * @return \Spatie\Fractal\Fractal
     */
    protected function transform(Order $order)
    {
        return fractal($order, new OrderTransformer)
            ->parseIncludes('customer');
    }

}
