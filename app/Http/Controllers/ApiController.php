<?php

namespace App\Http\Controllers;

use Exception;
use App\Helpers\Response\ResponseHelpers;

abstract class ApiController extends Controller
{
    use ResponseHelpers;

    /**
     * Log and return generic internal error.
     *
     * @param Exception $e
     * @return mixed
     */
    protected function logRespondGenericInternalError(Exception $e)
    {
        logger()->error($e->getMessage());

        return $this->respondGenericInternalError();
    }
}
