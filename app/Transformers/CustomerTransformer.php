<?php

namespace App\Transformers;

use App\Models\Customer;

class CustomerTransformer extends Transformer
{
    protected array $availableIncludes = [];

    /**
     * Transform Customer
     *
     * @param Customer $customer
     * @return array
     */
    public function transform(Customer $customer)
    {
        return [
            'name'             => $customer->name,
            'title'            => $customer->job_title,
            'email'            => $customer->email,
            'phone'            => $customer->phone,
            'registered_since' => format_time($customer->registered_since),
        ];
    }
}
