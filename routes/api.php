<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();

});

// Attach Product to Order
Route::post('orders/{order}/add', 'OrderController@addProduct')->name('add.order');

// Pay Order
Route::post('orders/{order}/pay', 'OrderController@payOrder')->name('pay.order');

// Order CRUD
Route::resource('/orders', \App\Http\Controllers\OrderController::class)->only([
    'index', 'store', 'update', 'destroy'
]);



