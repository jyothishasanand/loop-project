<?php

namespace App\Http\Requests;

class PayOrderRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:customers,email',
            'price' => 'required|numeric'
        ];
    }
}
