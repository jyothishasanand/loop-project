<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use League\Csv\Reader;
use App\Models\Product;
use App\Models\Customer;
use League\Csv\Exception;
use Illuminate\Console\Command;

class ImportMasterDataCommand extends Command
{
    const FILE_PATH = 'imports';
    const CUSTOMER_FILE_NAME = 'customers.csv';
    const PRODUCT_FILE_NAME = 'products.csv';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Master Data to tables products and customers.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->logIt("[Import: MasterData] Begin");

        $customerData = $this->prepareCustomerData();

        foreach (array_chunk($customerData,1000) as $data)
        {
            Customer::insert($data);
        }

        $productData = $this->prepareProductData();
        Product::insert($productData);

        $this->logIt("[Import: MasterData] End");
    }

    /**
     * Get data from CSV file.
     *
     * @param $filename
     * @return \Iterator
     * @throws Exception
     */
    protected function readCsv($filename)
    {
        $fileName = storage_path(file_path('app', file_path(self::FILE_PATH, $filename)));
        $csv = Reader::createFromPath($fileName, 'r');
        $csv->setHeaderOffset(0);

        return $csv->getRecords();
    }

    /**
     * Prepare Product Data.
     *
     * @return array
     * @throws \League\Csv\Exception
     */
    protected function prepareProductData()
    {
        $records = $this->readCsv(self::PRODUCT_FILE_NAME);

        foreach ($records as $key => $record) {

            $name = data_get($record, 'productname');
            $price = data_get($record, 'price');

            $position = $key++;

            if (empty($name)) {
                $this->logIt("Missing  product name at row {$position}");
                continue;
            }

            $data[] = [
                'name'       => $name,
                'price'      => $price,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        return $data;
    }


    /**
     * Prepare Customer Data.
     *
     * @return array
     * @throws \League\Csv\Exception
     */
    protected function prepareCustomerData()
    {
        $records = $this->readCsv(self::CUSTOMER_FILE_NAME);

        foreach ($records as $key => $record) {

            $email = data_get($record, 'email');
            $name = data_get($record, 'name');
            $name = explode(' ', $name);
            $firstName = data_get($name, 0);
            $lastName = data_get($name, 1);
            $registered = data_get($record, 'registered_since');
            $registeredSince = Carbon::parse($registered);
            $position = $key++;

            if (empty($email)) {
                $this->logIt("Missing customer email at row {$position}");
                continue;
            }

            $data[] = [
                        'email'            => $email,
                        'job_title'        => data_get($record, 'job_title'),
                        'first_name'       => $firstName,
                        'last_name'        => $lastName,
                        'registered_since' => $registeredSince,
                        'phone'            => data_get($record, 'phone'),
                        'created_at'       => now(),
                        'updated_at'       => now(),
                    ];
        }

        return $data;
    }


    /**
     * Log it and output it.
     *
     * @param $message
     */
    protected function logIt($message)
    {
        $this->getOutput()->writeln($message);
        info($message);
    }
}
