<?php

namespace App\Http\Requests;

class OrderRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'limit'       => 'sometimes|integer|min:5',
            'customer_id' => 'required|exists:customers,id',
            'payed'       => 'required|boolean'
        ];
    }
}
