<?php

namespace App\WebShop\Services\Http;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\BadResponseException;
use App\WebShop\Exceptions\HttpRequestException;

class HttpClient
{
    const RESPONSE_TYPE_JSON = 'json';
    const RESPONSE_TYPE_EMPTY = 'empty';

    /** @var \GuzzleHttp\Client */
    protected $client;

    /** @var array */
    protected $headers;

    /** @var array */
    protected $cookies;

    /** @var array */
    protected $queryString;

    /** @var array */
    protected $postBody;

    /** @var array */
    protected $jsonBody;

    /** @var string */
    protected $url;

    /** @var mixed */
    protected $timeout = 60;

    /** @var array */
    protected $auth = [];

    /**
     * SSL Details array.
     *
     * @var array */
    protected $ssl = [];

    /**
     * @var string
    */
    protected $responseType = self::RESPONSE_TYPE_JSON;

    /**
     * Multi-Part Details array.
     *
     * @var array */
    protected $multipart = [];

    /**
     * HttpClient constructor.
     *
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * HTTP Get Request.
     *
     * @param       $url
     * @param array $params
     * @param array $header
     *
     * @return array
     */
    public function get($url, $params = [], $header = [])
    {
        if ($this->hasUrlQueries($url)) {
            $queryParams = $this->getUrlQueries($url);
            $params = $this->addQueryParams($params, $queryParams);
        }

        return $this->setHeaders($header)->setQueryString($params)->sendRequest($url);
    }

    /**
     * HTTP Post Request.
     *
     * @param $url
     * @param array $params
     * @param array $header
     * @param array $cookies
     * @return array
     */
    public function post($url, $params = [], $header = [], $cookies = [])
    {
        return $this->setHeaders($header)->setCookies($cookies, $url)->setRequestBody($params)->sendRequest($url, 'POST');
    }

    /**
     * HTTP Put Request.
     *
     * @param       $url
     * @param array $params
     * @param array $header
     *
     * @return array
     */
    public function put($url, $params = [], $header = [])
    {
        return $this->setHeaders($header)->setRequestBody($params)->sendRequest($url, 'PUT');
    }

    /**
     * HTTP Delete Request.
     *
     * @param       $url
     * @param array $params
     * @param array $header
     *
     * @return array
     */
    public function delete($url, $params = [], $header = [])
    {
        return $this->setHeaders($header)->setRequestBody($params)->sendRequest($url, 'DELETE');
    }

    /**
     * HTTP Patch Request
     *
     * @param $url
     * @param $params
     * @param $header
     * @return array
     */
    public function patch($url, $params = [], $header = [])
    {
        return $this->setHeaders($header)->setRequestBody($params)->sendRequest($url, 'PATCH');
    }

    /**
     * Set the connection timeout.
     *
     * @param mixed $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * Set the request headers.
     *
     * @param array $headers
     *
     * @return $this
     */
    public function setHeaders($headers = [])
    {
        if (!empty($headers)) {
            $this->headers = $headers;
        }

        return $this;
    }

    /**
     * Set the request headers.
     *
     * @param array $cookies
     *
     * @param string $url
     * @return $this
     */
    public function setCookies(array $cookies, string $url)
    {
        if (!empty($cookies)) {
            $cookieJar = CookieJar::fromArray($cookies, data_get(parse_url($url), 'host'));
            $this->cookies = $cookieJar;
        }

        return $this;
    }

    /**
     * Sets a expected response type to 'JSON'.
     *
     * @param bool $default
     * @return $this
     */
    public function setJsonResponseType($default = true)
    {
        $this->responseType = $default ? self::RESPONSE_TYPE_JSON : null;

        return $this;
    }

    /**
     * Sets a expected response type to 'Empty'.
     *
     * @return $this
     */
    public function setEmptyResponseType()
    {
        $this->responseType = self::RESPONSE_TYPE_EMPTY;

        return $this;
    }

    /**
     * Set Request Auth values.
     * Username & password
     *
     * @param $username
     * @param $password
     * @return $this
     */
    public function setAuth($username, $password)
    {
        if (empty($username)) {
            return $this;
        }

        $this->auth = [
            'auth' => [$username, $password]
        ];

        return $this;
    }

    /**
     * Set Disable SSL Flag.
     *
     * @return $this
     */
    public function disableSSL()
    {
        $this->ssl = [
            'verify' => false
        ];

        return $this;
    }

    /**
     * Add Multipart data.
     *
     * @param array $data
     * @return $this
     */
    public function addMultipartData($data = [])
    {
        if (!empty($data)) {
            $this->multipart['multipart'] = $data;
        }

        return $this;
    }

    /**
     * Send the HTTP Request.
     *
     * @param        $url
     * @param string $method
     *
     * @return array
     */
    protected function sendRequest($url, $method = 'GET')
    {
        return $this->setUrl($url)->makeRequest($method);
    }

    /**
     * Make Http Request.
     *
     * @param $method
     *
     * @return array
     * @throws Exception
     */
    protected function makeRequest($method)
    {
        $response = ['code' => 200, 'message' => 'Success', 'data' => null];

        try {

            $additionalData = array_merge($this->multipart, $this->ssl);
            $parameters = array_merge([
                'query' => $this->getQueryString(),
                'form_params' => $this->getPostBody(),
                'json' => $this->getJsonBody(),
                'headers' => $this->headers,
                'connect_timeout' => $this->timeout,
                'cookies' => $this->cookies
            ], $this->auth, $additionalData);

            // Remove Form Params if the Multi Part data is provided.
            if (!empty($this->multipart)) {
                unset($parameters['form_params'], $parameters['json']);
            }

            //dd($parameters);
            $request = $this->client->request($method, $this->url, $parameters);
            $responseContentType = Arr::first($request->getHeader('Content-Type'));

            // Expecting JSON
            if ($this->responseType === self::RESPONSE_TYPE_JSON) {

                // Received Is XML
                if (substr_exist($responseContentType, 'text/xml')) {
                    $responseBody = $request->getBody()->getContents();
                    $xml = simplexml_load_string($responseBody);
                    $responseBody = json_encode($xml);
                }
            }
            // If Empty Response is expected
            elseif ($this->responseType === self::RESPONSE_TYPE_EMPTY) {

                // If Status code is 2xx, set the body = '{}' if body is empty.
                if (is_between($request->getStatusCode(), 200, 299)) {
                    $responseBody = $request->getBody()->getContents();
                    $responseBody = !empty($responseBody) ? $responseBody : '{}';
                }
            }

            $responseBody = $responseBody ?? $request->getBody()->getContents();
            $response['data'] = json_decode($responseBody, true);

            if ($this->responseType === self::RESPONSE_TYPE_JSON && !is_valid_json($responseBody, true) && !is_array($response['data'])) {
                $response['message'] = "Http Request Response is not a valid Json.";
                $response['data'] = (string) $responseBody;
                throw new HttpRequestException($response);
            }
        } catch (BadResponseException $e) {

            $responseContent = (string) $e->getResponse()->getBody();

            if (is_valid_json($responseContent)) {
                return [
                    'code'    => $e->getResponse()->getStatusCode(),
                    'message' => "Http Request resulted in a Bad Response with a code: " . $e->getCode() . ".",
                    'data'    => json_decode($responseContent, true, 512),
                ];
            }

            $response = [
                'code' => $e->getResponse()->getStatusCode(),
                'message' => "Http Request resulted in a Bad Response with a code: " . $e->getCode() . ".",
                'data' => $responseContent,
            ];

            throw new HttpRequestException($response);
        }

        return $response;
    }

    /**
     * Set the request url.
     *
     * @param string $url
     *
     * @return $this
     */
    protected function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Check for content type is set to 'application/json'.
     *
     * @return bool
     */
    protected function isJsonContentType()
    {
        return data_get($this->headers, 'Content-Type') == 'application/json';
    }

    /**
     * Set request body params.
     *
     * @param array $params
     *
     * @return $this
     */
    protected function setRequestBody($params = [])
    {
        if ($this->isJsonContentType()) {
            return $this->setJsonBody($params);
        }

        return $this->setPostBody($params);
    }

    /**
     * Get request query string params.
     *
     * @return array
     */
    protected function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * Set request query string params.
     *
     * @param array $queryString
     *
     * @return $this
     */
    protected function setQueryString($queryString = [])
    {
        $this->queryString = $queryString;

        return $this;
    }

    /**
     * Get request post body params.
     *
     * @return array
     */
    protected function getPostBody()
    {
        return $this->postBody;
    }

    /**
     * Set request post params.
     *
     * @param $postBody
     *
     * @return $this
     */
    protected function setPostBody($postBody = [])
    {
        $this->postBody = $postBody;

        return $this;
    }

    /**
     * Get request json body params.
     *
     * @return array
     */
    protected function getJsonBody()
    {
        return $this->jsonBody;
    }

    /**
     * Set request json body params.
     *
     * @param array $jsonBody
     *
     * @return $this
     */
    protected function setJsonBody($jsonBody = [])
    {
        $this->jsonBody = $jsonBody;

        return $this;
    }

    /**
     * Sets request parameters from url queries.
     *
     * @param array $params
     * @param array $urlQueries
     * @return array
     */
    protected function addQueryParams($params, $urlQueries)
    {

        foreach ($urlQueries as $key => $parameter) {
            if (!array_key_exists($key, $params)) {
                $params[$key] = $parameter;
            }
        }

        return $params;
    }

    /**
     * Checks if a given url has any query parameters.
     *
     * @param string $url
     * @return bool
     */
    protected function hasUrlQueries($url)
    {
        return (bool) count($this->getUrlQueries($url));
    }

    /**
     * Get url queries as an array.
     *
     * @param string $url
     * @return array
     */
    protected function getUrlQueries($url)
    {
        $queryUrl = parse_url($url, PHP_URL_QUERY) ?? '';
        parse_str($queryUrl, $queryParams);

        return $queryParams;
    }
}
