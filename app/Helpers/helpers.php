<?php

use App\WebShop\Services\Http\HttpClient;

if (! function_exists('http_client')) {
    /**
     * Get HTTP Client
     *
     * @return HttpClient
     */
    function http_client()
    {
        return app(HttpClient::class);
    }
}

if (! function_exists('substr_exist')) {
    /**
     * Check if a given substring exists in the given string - Case sensitive.
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $caseSensitive
     * @return bool
     */
    function substr_exist($haystack, $needle, $caseSensitive = false)
    {
        if (!$caseSensitive) {
            $haystack = strtolower($haystack);
            $needle = strtolower($needle);
        }

        return !is_bool(strpos($haystack, $needle));
    }
}

if (! function_exists('is_valid_json')) {
    /**
     * Check if the input is a valid JSON data.
     *
     * @param string $dataString
     * @param bool $isObjectOrArray
     * @return bool
     */
    function is_valid_json($dataString, bool $isObjectOrArray = false)
    {
        if (empty($dataString)) {
            return false;
        }

        $decoded = json_decode($dataString);
        $isValidJson = (!empty($decoded) ? true : false) && json_last_error() == JSON_ERROR_NONE;
        if ($isValidJson && $isObjectOrArray) {
            return !is_int($decoded);
        }

        return $isValidJson;
    }
}

if (! function_exists('file_path')) {
    /**
     * Get the full file path given the folder path and file name.
     *
     * @param string $path
     * @param string $filename
     * @param string $folder The folder inside the path
     * @return string
     */
    function file_path($path, $filename, $folder = null)
    {
        return rtrim($path, '/') . ($folder ? "/{$folder}/" : '/') . $filename;
    }
}

/**
 * Get the Query String Filter instance.
 *
 * @param \Illuminate\Database\Eloquent\Builder|null $builder
 * @param \League\Fractal\TransformerAbstract|callable|null $transformer
 * @param \App\WebShop\Libraries\QueryFilter\FilterContract|null $customFilter
 * @return \App\WebShop\Libraries\QueryFilter\QueryFilter
 */
function filter(\Illuminate\Database\Eloquent\Builder $builder = null, $transformer = null, \App\WebShop\Libraries\QueryFilter\FilterContract $customFilter = null)
{
    $queryFilter = app('query-filter');

    if (!is_null($builder)) {
        $queryFilter = $queryFilter->builder($builder);
    }

    if (!is_null($transformer)) {
        $queryFilter = $queryFilter->transformWith($transformer);
    }

    if (!is_null($customFilter)) {
        $queryFilter = $queryFilter->customFilter($customFilter);
    }

    return $queryFilter;
}

if (! function_exists('limit_value')) {
    /**
     * Limit the given input value between the min and max value.
     *
     * @param mixed $value
     * @param mixed $min
     * @param mixed $max
     * @return mixed
     */
    function limit_value($value, $min, $max)
    {
        return min(max($value, $min), $max);
    }
}

/**
 * Get the human readable datetime from unix-timestamp
 *
 * @param $datetime
 * @param string $format
 * @return false|null|string
 */
function format_time($datetime, $format = 'Y-m-d H:i:s')
{
    if (is_null($datetime)) {
        return null;
    }

    if (!empty($datetime) && is_object($datetime) && $datetime instanceof \Carbon\Carbon) {
        return $datetime->format($format);
    }

    $timestamp = is_numeric($datetime) ? $datetime : strtotime($datetime);
    if ($timestamp !== false && $timestamp < 0) {
        return null;
    }

    return !empty($datetime)
        ? date($format, $timestamp ?? $datetime)
        : null;
}

if (! function_exists('resolve_boolean')) {

    /**
     * Resolves a string towards a boolean value.
     *
     * @param $value
     * @return bool
     */
    function resolve_boolean($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
}

if (! function_exists('app_debug_enabled')) {
    /**
     * Check if the app debug is enabled.
     *
     * @return bool
     */
    function app_debug_enabled()
    {
        return config('app.debug', false);
    }
}

if (! function_exists('is_between')) {
    /**
     * Check if a give number is between two numbers.
     *
     * @param $num
     * @param $min
     * @param $max
     * @return bool
     */
    function is_between($num, $min, $max)
    {
        return ($num >= $min) && ($num <= $max);
    }
}


