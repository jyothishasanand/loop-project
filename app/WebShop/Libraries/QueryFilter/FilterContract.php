<?php

namespace App\WebShop\Libraries\QueryFilter;

interface FilterContract
{
    /**
     * The available filters.
     *
     * @return array
     */
    public function filters();
}
