<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use App\WebShop\Exceptions\CustomValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \App\WebShop\Exceptions\CustomValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  Throwable $exception
     * @return void
     *
     * @throws Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Throwable $exception
     * @return \Illuminate\Http\Response|JsonResponse
     * @throws \Exception
     */
    public function render($request, Throwable $exception)
    {
        if ($this->expectsJson($request)) {
            return $this->getJsonResponse($exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * Check if the current request expects a JSON response.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function expectsJson($request)
    {
        if ($request->expectsJson()) {
            return true;
        }

        return (!empty($request->segments()) && $request->segments()[0] === 'api');
    }

    /**
     * Generate a Json Response for the given Exception.
     *
     * @param Throwable $exception
     * @return JsonResponse
     */
    protected function getJsonResponse(Throwable $exception)
    {
        $exception = $this->prepareException($exception);

        $statusCode = $this->getStatusCode($exception);

        if ($exception instanceof NotFoundHttpException || !($message = $exception->getMessage())) {
            $message = ($exception->getPrevious() instanceof ModelNotFoundException)
                ? $exception->getMessage()
                : sprintf('%d %s', $statusCode, Response::$statusTexts[$statusCode]);
        }

        if ($exception instanceof QueryException && !$this->runningInDebugMode()) {
            $message = 'Internal Server Error';
        }

        $data = [
            'success' => false,
            'message' => $message,
            'status_code' => $statusCode,
        ];

        if ($exception instanceof ValidationException || $exception instanceof CustomValidationException) {
            $data['status_code'] = $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data['errors'] = $exception instanceof ValidationException ?
                $exception->validator->errors()->getMessages() : $exception->getMessages();
        }

        if ($exception->getCode() == Response::HTTP_PRECONDITION_REQUIRED) {
            $data['status_code'] = $statusCode = Response::HTTP_PRECONDITION_REQUIRED;
            $data['errors'] = [ 'condition' => $exception->getMessage() ];
        }

        if ($code = $exception->getCode()) {
            $data['code'] = $code;
        }

        if ($this->runningInDebugMode()) {
            $data['debug'] = [
                'line' => $exception->getLine(),
                'file' => $exception->getFile(),
                'class' => get_class($exception),
                'trace' => explode('\n', $exception->getTraceAsString()),
            ];
        }

        return response()->json($data, $statusCode);
    }

    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpExceptionInterface $e)
    {
        $this->registerErrorViewPaths();

        if (view()->exists($view = $this->getHttpExceptionView($e))) {

            $viewData = [
                        'errors' => new ViewErrorBag,
                        'exception' => $e,
                    ];

            return response()->view($view, $viewData, $e->getStatusCode(), $e->getHeaders());
        }

        return $this->convertExceptionToResponse($e);
    }

    /**
     * Get the exception status code
     *
     * @param Throwable $exception
     * @param int $defaultStatusCode
     * @return int
     */
    protected function getStatusCode(Throwable $exception, $defaultStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        if ($this->isHttpException($exception)) {
            return $exception->getStatusCode();
        }

        return $exception instanceof AuthenticationException ?
            Response::HTTP_UNAUTHORIZED :
            $defaultStatusCode;
    }

    /**
     * Check if the application is running with debug enabled.
     *
     * @return bool
     */
    protected function runningInDebugMode()
    {
        return app_debug_enabled();
    }

}
