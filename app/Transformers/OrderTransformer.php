<?php

namespace App\Transformers;

use App\Models\Order;

class OrderTransformer extends Transformer
{

    protected array $availableIncludes = [
        'customer',
    ];

    /**
     * Transform Orders
     *
     * @param Order $order
     * @return array
     */
    public function transform(Order $order)
    {

        return [
            'order_id'   => $order->id,
            'created_at' => format_time($order->created_at),
            'payed'      => $order->payed
        ];
    }


    /**
     * Include the Customer.
     *
     * @param Order $order
     * @return \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
     */
    public function includeCustomer(Order $order)
    {
        $customer = $order->customer;

        return $customer
            ? $this->item($customer, new CustomerTransformer)
            : $this->null();
    }

}
