<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * Get email rule.
     *
     * @param bool $required
     * @return string
     */
    protected function emailRule(bool $required = true)
    {
        return ($required ? 'required|' : 'sometimes|') . 'email|max:150';
    }

    /**
     * Get Category rule.
     *
     * @param bool $required
     * @param bool $active
     * @return string
     */
    protected function categoryRule(bool $required = true, $active = true)
    {
        return ($required ? 'required|' : 'sometimes|')
                    . 'exist:categories,id'
                    . ($active ? ',status,1' : '');
    }


    /**
     * Integer is like a whole number without fraction: 2, 256, 2048
     *
     * @param bool $required
     * @param null $additionalValidations
     * @return string
     */
    protected function wholeNumber($required = false, $additionalValidations = null)
    {
        $required = $required ? 'required' : 'sometimes';
        $additionalValidations = !empty($additionalValidations)
            ? trim($additionalValidations, '|')
            : null;

        return "{$required}|integer" . ($additionalValidations ? "|{$additionalValidations}" : null);
    }

    /**
     * Numeric is any number including floating point numbers: 2.478, +0123.45e6
     *
     * @param bool $required
     * @param null $additionalValidations
     * @return string
     */
    protected function decimals($required = false, $additionalValidations = null)
    {
        $required = $required ? 'required' : 'sometimes';
        $additionalValidations = !empty($additionalValidations)
            ? trim($additionalValidations, '|')
            : null;

        return "{$required}|numeric" . ($additionalValidations ? "|{$additionalValidations}" : null);
    }

    /**
     * Get duration validation rule.
     *
     * @param bool $required
     * @param null $additionalValidations
     * @return string
     */
    protected function duration($required = false, $additionalValidations = null)
    {
        $required = $required ? 'required' : 'sometimes';
        $additionalValidations = !empty($additionalValidations)
            ? trim($additionalValidations, '|')
            : null;

        return "{$required}|string|duration" .
            ($additionalValidations ? "|{$additionalValidations}" : null);
    }

    /**
     * Get Allowed Mime Types for File Uploads
     *
     * @return string
     */
    protected function allowedFileMimes()
    {
        return implode(',', config('courses.uploads.file.allowed_mime'));
    }

    /**
     * Get Max File Size
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function allowedFileSize()
    {
        return config('courses.uploads.file.max_file_size_kb', 1024);
    }

    /**
     * Get Logo Dimensions.
     *
     * @return array
     */
    protected function allowedLogoDimensions()
    {
        return [
            'min_height=200',
            'max_height=1024',
            'min_width=200',
            'max_width=1024',
        ];
    }
}
